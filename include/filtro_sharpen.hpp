#ifndef FILTRO_SHARPEN_H
#define FILTRO_SHARPEN_H
#include "imagem.hpp"
#include "filtro.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 

using namespace std;

class sharpen : public filtro{
	public:
		void aplicar_efeito_sharpen(Imagem &imagemLena,int v1, int v2);

};

#endif
