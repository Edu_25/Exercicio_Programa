#ifndef IMAGEM_H
#define IMAGEM_H
#include<iostream> //Implementa uma hierarquia de classes OO
#include<string> //Para usar char(nao e exatamente isto,mas so para dar uma ideia)
#include<fstream> //Para realizar E/S de arquivos
#include<sstream> //Ele permite ler e escrever em modo de texto
#include<stdlib.h> //Responsavel pela conversao de numeros, as alocalocacoes de memoria e outras funcoes

using namespace std; //Para não ter que ficar fazendo referencia direto std::algumacoisa

/*Essa classe permitirá o adimicao da imagem, saber se ela e do tipo PGM, desconsiderar os comentarios para que possa fazer a manipulacao da mesma*/
using namespace std;

class Imagem{
private:
	//atributos
	int linhas;
	int colunas;
	int cinza;
	int *matriz;//vetor que trabalhará com os pixels da imagem
public:
	Imagem(); //construtor
	Imagem(int linhas, int colunas, int cinza);
	
	void lerImagem(const char arq_entr[]);	
	void salvarImagem(const char arq_said[]);	

	int getLinhas();
	int getColunas();
	int getCinza();

	void setLinhas(int linhas);
	void setColunas(int colunas);
	void setCinza(int cinza);	

	int getPixel(int linha, int coluna);
	void setPixel(int linha, int coluna, int valor);	
	~Imagem();//desconstrutor da classe
};
#endif
