#ifndef NEGATIVO_H
#define NEGATIVO_H
#include "imagem.hpp"
#include "filtro.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 

using namespace std;

class negativo : public filtro {
	public:
		void aplicar_efeito_negativo(Imagem &imagemLena);		
};

#endif
