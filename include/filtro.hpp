#ifndef FILTRO_h
#define FILTRO_h

using namespace std;

#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h> 
#include <sstream>

using namespace std;

class filtro {
	private:
		//estas duas variáveis já estão presentes nos códigos dos filtros smooth e sharpen;
		int v1;
		int v2;
	public:
		filtro();
		filtro(int v1, int v2);
		int getV1();
		int getV2();
		void setV1(int v1);
		void setV2(int V2);

};
#endif
