#ifndef FILTRO_SMOOTH_H
#define FILTRO_SMOOTH_H
#include "imagem.hpp"
#include "filtro.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 

using namespace std;

class smooth : public filtro{
	public:
		void aplicar_efeito_smooth(Imagem &imagemLena,int v1,int v2);

};
#endif
