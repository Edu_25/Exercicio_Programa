#include "filtro_sharpen.hpp"
#include "filtro.hpp"

using namespace std;

void sharpen::aplicar_efeito_sharpen(Imagem &imagemLena,int v1, int v2){

		int sharpen[] = {0, -1, 0, -1, 5, -1, 0, -1, 0};
		int linhas, colunas, valor, i,j;
		
		linhas = imagemLena.getLinhas();
		colunas = imagemLena.getColunas();

		int *m = new int[linhas*colunas];
		
		for (i=v2/2; i < linhas-v2/2; i++)
		{
			for (j = v2/2; j < colunas-v2/2; j++)
			{
				valor = 0;
				for(int x = -1; x<=1; x++)
				{		
					for(int y = -1; y<=1; y++)
					{		
						valor += sharpen[(x+1)+ v2*(y+1)] *
					    imagemLena.getPixel(i+x, y+j);
						
					}
				}
				
				valor /= v1;			
				valor= valor < 0 ? 0 : valor;
				valor=valor >255 ? 255 : valor;
				
				m[i+colunas*j] = valor;
			}
		}
		
		for(i=0;i<linhas; i++){
			for(j=0; j<colunas; j++)
			{
				imagemLena.setPixel(i, j, m[i+colunas*j]);

			}
		}
}
