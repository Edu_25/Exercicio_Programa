#include "imagem.hpp"
#include "filtro.hpp"
#include "filtro_negativo.hpp"
#include "filtro_sharpen.hpp"
#include "filtro_smooth.hpp"
#include<string>
#include<iostream>

using namespace std;

int main(){

	//Objetos das respectivas classes
	Imagem img_Lena;
	sharpen op_sharpen;
	negativo op_negativo;
	smooth op_smooth;
	string efeito,resposta;
	int verifica = 0,verifica_1 = 0;
	img_Lena.lerImagem("../images/lena.pgm");
	
	do{
cout << "Com este programa voce pode aplicar 3 tipos de filtros a uma imagem do tipo PGM: "<< endl << "Sharpen, Negativo e Smooth" << endl;
	cout << "Qual desejas aplicar? (Por favor, escrever de forma extensa)" << endl;
	 
	cin >> efeito;
 	
	if(efeito == "Sharpen" || efeito == "sharpen") 
	{
		op_sharpen.setV1(1);
		op_sharpen.setV2(3);
		op_sharpen.aplicar_efeito_sharpen(img_Lena, op_sharpen.getV1(), op_sharpen.getV2());
		img_Lena.salvarImagem("../images/lena_com_filtro_Sharpen.pgm");
		verifica = 1;
	}
	else if(efeito == "Smooth" || efeito == "smooth") 
	{
		op_smooth.setV1(9);
		op_smooth.setV2(3);
		op_smooth.aplicar_efeito_smooth(img_Lena,op_smooth.getV1(), op_smooth.getV2());
		img_Lena.salvarImagem("../images/lena_com_filtro_Smooth.pgm");	
		verifica = 1;
	}	
	else if(efeito == "Negativo" || efeito == "negativo") 
	{
		op_negativo.aplicar_efeito_negativo(img_Lena);
		img_Lena.salvarImagem("../images/lena_com_filtro_Negativo.pgm");
		verifica = 1;
	}
	
	else{
	cout << "Desculpe, a entrada não corresponde a nenhum dos filtros listados" << endl;
	verifica_1 = 0;	
	while(verifica_1 != 1){	
	cout << "Deseja, tentar novamente? (Se sim, favor inserir 'Sim', caso contrário, favor inserir 'Não')" << endl;
	cin >> resposta;

		if(resposta == "Sim" || resposta == "sim")	
		{
			verifica = 0;
			verifica_1 = 1;
		}
		else if(resposta == "Nao" || resposta == "nao" || resposta == "Não" || resposta == "não")
		{
			verifica = 1;
			verifica_1 = 1;
		}
		else{
			cout << "Sua resposta não corresponde a pergunta!!!" << endl;
			verifica = 1;
			verifica_1 = 0; 
		}
	}  
	}
	
}while(verifica != 1);
	return 0;
}

