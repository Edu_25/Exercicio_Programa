#include "imagem.hpp"

using namespace std;
Imagem::Imagem() {
	linhas = 0;
	colunas = 0;
	cinza = 0;
	matriz = NULL;
}

void Imagem::lerImagem(const char arq_entr[]){

	ifstream arquivo;	
	int i,j,valor;
	char cabecalho [100], *ptr;
	unsigned char *charImage;
        

	arquivo.open(arq_entr, ios::in | ios::binary);
	
    	if (!arquivo) 
    	{
        cout << "Desculpe, não foi possivel ler a imagem!!! " << arq_entr << endl;
        exit(1);
    	}

       //Verificacao de ser PGM    
	arquivo.getline(cabecalho,100,'\n');
	
	if ( (cabecalho[0]!='P') || (cabecalho[1]!='5') )
        {   
        	cout << "O arquivo: " << arq_entr << " não e um arquivo PGM!!!" << endl;
         	exit(1);
        }

    //Comentarios #
    	arquivo.getline(cabecalho,100,'\n');
    	while(cabecalho[0]=='#')
	{
        	arquivo.getline(cabecalho,100,'\n');
	}
    
    	linhas = strtol(cabecalho,&ptr,0);
    	colunas = atoi(ptr);
    
    	arquivo.getline(cabecalho,100,'\n');
    	cinza = strtol(cabecalho,&ptr,0);
    	matriz = new int[linhas*colunas];

	charImage = (unsigned char *) new unsigned char [linhas*colunas];

        arquivo.read( reinterpret_cast<char *>(charImage), (linhas*colunas)*sizeof(unsigned char));

    	if (arquivo.fail()) 
    	{
        	cout << "Imagem " << arq_entr << " é do tamanho errado" << endl;
        	exit(1);
    	}

    	arquivo.close();

    	for(i = 0; i < linhas; i++)
	{
        	for(j = 0; j < colunas; j++) 
        	{
	            valor = (int)charImage[i*colunas+j];
	            matriz[i*colunas+j] = valor;
		}   
        }     

     delete [] charImage;
}
Imagem::Imagem(int linhas, int colunas, int cinza) {
    this->linhas = linhas;
    this->colunas = colunas;
    this->cinza = cinza;
    matriz = new int[linhas*colunas];
}

Imagem::~Imagem(){
	delete []matriz;
}

int Imagem::getLinhas() {
	return linhas;	
}

int Imagem::getColunas() {
	return colunas;
}

int Imagem::getCinza() {
	return cinza;
}

void Imagem::setLinhas(int linhas) {
	this->linhas=linhas;	
}

void Imagem::setColunas(int colunas) {
	this->colunas = colunas;
}

void Imagem::setCinza(int cinza) {
	this->cinza = cinza;
}



 void Imagem::salvarImagem(const char arq_said[]) {
	int i, j;
	unsigned char *charImage;
	ofstream outfile(arq_said);
	
	charImage = (unsigned char *) new unsigned char [linhas*colunas];
	
	int valor;

        for(i=0; i<linhas; i++)
    	{
        	for(j = 0; j<colunas; j++) 
        	{
			valor = matriz[i*colunas+j];
			charImage[i*colunas+j]=(unsigned char)valor;
        	}
    	}
    
	if (!outfile.is_open())
	{
		cout << "Desculpe, não foi possivel abrir o arquivo"  << arq_said << endl;
		exit(1);
	}
	
	outfile << "P5" << endl;
    	outfile << linhas << " " << colunas << endl;
    	outfile << 255 << endl;
	
	outfile.write(reinterpret_cast<char *>(charImage), (linhas*colunas)*sizeof(unsigned char));
	cout << "Filtro aplicado com sucesso!!!" << endl;
	outfile.close();
}

int Imagem::getPixel(int linha, int coluna)
{
	int x = getColunas();
	return matriz[linha*x+coluna];
}

void Imagem::setPixel(int linha, int coluna, int valor)
{
	int x = getColunas();
	matriz[linha*x+coluna] = valor;
}

