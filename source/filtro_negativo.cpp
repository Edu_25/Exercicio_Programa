#include "filtro_negativo.hpp"
#include "filtro.hpp"

using namespace std;

void negativo::aplicar_efeito_negativo(Imagem &imagemLena) {
		
		int linhas, colunas; 
		int i,j; //Contadores do for

		linhas = imagemLena.getLinhas();
		colunas = imagemLena.getColunas();
		
		for (i = 0; i < linhas; i++)
		{
			for (j = 0; j < colunas; j++)
			{
				imagemLena.setPixel(i,j, 255-imagemLena.getPixel(i, j) );
			}
		} 
}
